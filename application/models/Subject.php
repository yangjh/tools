<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * 股票数据模型
 */
class Subject extends CI_model {

	public $subjectsOutline = array(
		"社会调查原理与方法" => 'research.json',
		"市场调查与分析"   => 'market.json',
		"PHP网站开发"   => "PHP.json",
		"网页设计与制作"   => "front-end.json",
	);

	// 计算指定日期所在的教学周编号
	public function getweekid($date = '') {
		// 计算时间差 / 7 取整
		$d0     = strtotime($_POST['startdate']);
		$d1     = strtotime($date);
		$weekid = round(($d1 - $d0) / 3600 / 24 / 7) + 1;
		return $weekid;
	}

	// 获取指定课程内容
	public function getoutline($subject = '') {
		$fileName    = './application/tmp/' . $this->subjectsOutline[$subject];
		$json_string = file_get_contents($fileName);
		return json_decode($json_string, true);
	}

	public function getdata36() {

		$data = array(
			'subject' => $_POST['subject'],
		);

		$data['info'] = array();

		$weekarray = array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六");

		// 确定第一次课程时间 = 开始时间 + 星期几 - 1
		// echo date('YY-MM-DD', strtotime($_POST['startdate']));
		$diff = (int) $_POST['week'] - 1;
		// 获取课程内容
		$contents = $this->getoutline($_POST['subject']);
		for ($i = 0; $i < 18; $i++) {
			// echo date('Y-m-d', strtotime('+' . $diff + $i * 7 . ' day', strtotime($_POST['startdate'])));
			// echo '<br>';
			// 进度编号
			$data['info'][$i]['id'] = $i + 1;
			// 课程进度日期
			$data['info'][$i]['date'] = date('Y-m-d', strtotime('+' . $diff + $i * 7 . ' day', strtotime($_POST['startdate'])));
			// 课程进度日期对应的星期
			$data['info'][$i]['week'] = $weekarray[date("w", strtotime($data['info'][$i]['date']))];
			// 课程进度日期所在的教学周
			$data['info'][$i]['weekid'] = $this->getweekid($data['info'][$i]['date']);
			// 课程具体节次
			$data['info'][$i]['dayseg1'] = $_POST['dayseg1'];
			// 课程地点
			$data['info'][$i]['location1'] = $_POST['location1'];
			// 课程内容
			$data['info'][$i]['content'] = $contents[$i];
		}
		return $data;
	}

	public function getdata54() {

		$data = array(
			'subject' => $_POST['subject'],
		);

		$data['info'] = array();

		$weekarray = array("星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六");

		// 确定第一次课程时间 = 开始时间 + 星期几 - 1
		// echo date('YY-MM-DD', strtotime($_POST['startdate']));
		$diff1 = (int) $_POST['week1'] - 1;
		$diff2 = (int) $_POST['week2'] - 1;
		$diff3 = (int) $_POST['week3'] - 1;
		// 获取课程内容
		$contents = $this->getoutline($_POST['subject']);
		$k        = 0;
		for ($i = 0; $i < 18; $i++) {
			if (($i + 1) % 2 == 1) {
				// 先计算单周
				for ($j = 1; $j <= 3; $j++) {
					// 每个安排计算一次
					if ($_POST['isOddWeek' . $j] == 1) {
						$data['info'][$k]['id'] = $k + 1;
						// 课程进度日期
						$var                      = 'diff' . $j;
						$data['info'][$k]['date'] = date('Y-m-d', strtotime('+' . $$var + $i * 7 . ' day', strtotime($_POST['startdate'])));
						// 课程进度日期对应的星期
						$data['info'][$k]['week'] = $weekarray[date("w", strtotime($data['info'][$k]['date']))];
						// 课程进度日期所在的教学周
						$data['info'][$k]['weekid'] = $this->getweekid($data['info'][$k]['date']);
						// 课程具体节次
						$data['info'][$k]['dayseg1'] = $_POST['dayseg' . $j];
						// 课程地点
						$data['info'][$k]['location1'] = $_POST['location' . $j];
						// 课程内容
						$data['info'][$k]['content'] = $contents[$k];
						$k                           = $k + 1;
					}
				}
			} else {
				for ($j = 1; $j <= 3; $j++) {
					// 每个安排计算一次
					if ($_POST['isOddWeek' . $j] == 2) {
						// 序号
						$data['info'][$k]['id'] = $k + 1;
						// 课程进度日期
						$var                      = 'diff' . $j;
						$data['info'][$k]['date'] = date('Y-m-d', strtotime('+' . $$var + $i * 7 . ' day', strtotime($_POST['startdate'])));
						// 课程进度日期对应的星期
						$data['info'][$k]['week'] = $weekarray[date("w", strtotime($data['info'][$k]['date']))];
						// 课程进度日期所在的教学周
						$data['info'][$k]['weekid'] = $this->getweekid($data['info'][$k]['date']);
						// 课程具体节次
						$data['info'][$k]['dayseg1'] = $_POST['dayseg' . $j];
						// 课程地点
						$data['info'][$k]['location1'] = $_POST['location' . $j];
						// 课程内容
						$data['info'][$k]['content'] = $contents[$k];
						$k                           = $k + 1;
					}
				}
			}
		}
		return $data;
	}
}
