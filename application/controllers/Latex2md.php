<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Latex2md extends CI_Controller {

	public $defalutCode = 'python';

	public function __construct() {
		// 继承父类构造方法
		parent::__construct();
		// 装载相关数据模型
		$this->load->model('Subject');
	}

	public function index() {
		// 读取cookie信息
		$data['defalutCode'] =  get_cookie('defalutCode');
		$this->load->view('latex2md.html',$data);
	}
	// 转换数据
	public function transform() {
		// 接收数据
		$content           = $this->input->post('tex');
		$this->defalutCode = $this->input->post('defalutCode');
		set_cookie('defalutCode',$this->defalutCode);
		// var_dump($this->input->post('defalutCode'));exit();
		// 转化为数组
		$content = explode("\n", $content);
		// 逐一进行处理
		foreach ($content as $key => $value) {
			$content[$key] = $this->tex2md($key, $content[$key]);
		}
		// 合并数组
		$data['content'] = implode("\n", $content);
		// 删除多余空行
		$data['content'] = $this->removeEmptyLine($data['content']);
		// 显示处理之后的数据
		$data['defalutCode'] =  $this->defalutCode;
		$this->load->view('latex2md.html', $data);
	}

	// 更换超级链接表现方式
	public function texhref2md($value = '') {
		/*
			这里为什么是四个反斜线，原因在于：我们知道在正则概念上转义反斜线的写法为："\\" 这个在正则表式下是能匹配出\ 即 /\/，这是正则表达式引擎拿到的模式，但你也注意到了匹配出的\ 会转义后面的分隔符即：\/ ，所以我还需要一个反斜线来转义这个\ 。这样四个反斜线可理解为:前两个"生成"的\ 转义后两个"生成"的反斜杠。
		*/

		$pattern     = '/\\\\href{(.*?)\}\{(.*?)\}/';
		$replacement = '[$2]($1)';
		return preg_replace($pattern, $replacement, $value);
	}

	// 更换列表表现方式
	public function texlist2md($value = '') {
		$pattern     = '/(\\\\begin\{enumerate\})|(\\\\end\{enumerate\})|(\\\\begin\{itemize\})|(\\\\end\{itemize\})/';
		$replacement = "";
		return preg_replace($pattern, $replacement, $value);
	}

	// 更换列表表现形式
	public function texitem2md($key = 0, $value = '') {
		$pattern     = '/\s*\\\\item/';
		$replacement = "$key.";
		return preg_replace($pattern, $replacement, $value);
	}

	// 更换页脚表现方式
	public function texfoot2md($key = 0, $value = '') {
		$pattern     = '/\\\\footnote{(.*?)\}/';
		$replacement = "[^$key]" . "\n" . "[^$key]: $1";
		return preg_replace($pattern, $replacement, $value);
	}

	// 更换标题表现方式
	public function texsection2md($value = '') {
		$pattern     = '/\\\\section{(.*?)\}/';
		$replacement = "## $1";
		return preg_replace($pattern, $replacement, $value);
	}

	// 更换表现方式
	public function tex2md($key = 0, $value = '') {
		$pattern     = array();
		$replacement = array();
		// 一级标题
		$pattern[0]     = '/\\\\chapter{(.*?)\}/';
		$replacement[0] = "\n" . "# $1" . "\n";
		// 二级标题
		$pattern[1]     = '/\\\\section{(.*?)\}/';
		$replacement[1] = "\n" . "## $1" . "\n";
		// 三级标题
		$pattern[2]     = '/\\\\subsection{(.*?)\}/';
		$replacement[2] = "\n" . "### $1" . "\n";
		// href
		$pattern[3]     = '/\\\\href{(.*?)\}\{(.*?)\}/';
		$replacement[3] = '[$2]($1)';
		// list
		$pattern[4]     = '/(\\\\begin\{enumerate\})|(\\\\end\{enumerate\})|(\\\\begin\{itemize\})|(\\\\end\{itemize\})|(\\\\begin\{description\})|(\\\\end\{description\})|(\\\\begin\{myremark\}\{.*?\})|(\\\\end\{myremark\})|(\\\\bibliography\{.*?\})|(\\\\bibliographystyle\{.*?\})/';
		$replacement[4] = "";
		// dd item
		$pattern[5]     = '/\s*\\\\item\s*\[(.*?)\]/';
		$replacement[5] = "$key. **$1**";

		// footnote
		$pattern[6]     = '/\\\\footnote{(.*?)\}/';
		$replacement[6] = "[^$key]" . "\n" . "[^$key]: $1";
		// cite
		$pattern[7]     = '/\\\\cite{(.*?)\}/';
		$replacement[7] = '{{ "$1" | cite }}';
		// subsubsection
		$pattern[8]     = '/\\\\subsubsection{(.*?)\}/';
		$replacement[8] = "\n" . "#### $1" . "\n";
		// label
		$pattern[9]     = '/\\\\label{.*?\}/';
		$replacement[9] = "";
		// comment
		$pattern[10]     = '/^%.*/';
		$replacement[10] = "";
		// coding
		$pattern[11]     = '/\\\\begin\{lstlisting\}/';
		$replacement[11] = "``` " . $this->defalutCode;
		$pattern[12]     = '/\\\\end\{lstlisting\}/';
		$replacement[12] = "```";
		// url
		$pattern[13]     = '/\\\\url\{(.*?)\}/';
		$replacement[13] = "<$1>";
		// item
		$pattern[14]     = '/\s*\\\\item/';
		$replacement[14] = "$key.";
		// item
		// \lstinputlisting[language=PHP,emph={new,class},emphstyle=\bfseries]{example/PHP/05-oop1/01.php}
		$pattern[15]     = '/\\\\lstinputlisting\[.*?\]{(.*?)}|\\\\lstinputlisting{(.*?)}/';
		$replacement[15] = "[import]($1)";

		return preg_replace($pattern, $replacement, $value);
	}

	// 删除多余空行
	public function removeEmptyLine($value = '') {
		$pattern     = array();
		$replacement = array();

		// (fold)
		$pattern[1]     = '/.*?\(fold\).*?/';
		$replacement[1] = "";

		$pattern[0]     = '/[\n\r]{3,}/';
		$replacement[0] = "\n\n";

		return preg_replace($pattern, $replacement, $value);
	}

}
