<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teachinglog extends CI_Controller {

	public function __construct() {
		// 继承父类构造方法
		parent::__construct();
		// 装载相关数据模型
		$this->load->model('Subject');
	}

	public function index() {
		// 加载工具视图
		// echo '在这里，我将开始编写我的工具';
		$this->load->view('teachinglog.html');
	}

	public function generation() {
		if ((int) $_POST['totalclasshour'] == 36) {
			$data = $this->Subject->getdata36();
		} elseif ((int) $_POST['totalclasshour'] == 54) {
			$data = $this->Subject->getdata54();
		} else {

		}
		$this->load->view('subject.html', $data);
	}

}
