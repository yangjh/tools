<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Random extends CI_Controller {

	public function index($amount = 110) {
		// echo '生成全校宿舍号名单';
		$dorm[1]  = $this->building(1, 6, 26);
		$dorm[2]  = $this->building(2, 6, 26);
		$dorm[3]  = $this->building(3, 6, 26);
		$dorm[4]  = $this->building(4, 6, 26);
		$dorm[5]  = $this->building(5, 6, 26);
		$dorm[6]  = $this->building(6, 6, 26);
		$dorm[7]  = $this->building(7, 6, 26);
		$dorm[8]  = $this->building(8, 6, 26);
		$dorm[9]  = $this->building(9, 6, 26);
		$dorm[10] = $this->building(10, 6, 26);
		$dorm[11] = $this->building(11, 6, 26);
		$dorm[12] = $this->building(12, 6, 26);
		$dorm[13] = $this->building(13, 6, 26);
		$dorm[14] = $this->building(14, 6, 26);
		$dorm[15] = $this->building(15, 6, 26);
		$dorm[16] = $this->building(16, 6, 26);
		$dorm[17] = $this->building(17, 6, 26);
		$dorm[18] = $this->building(18, 12, 44);
		$dorm[19] = $this->building(19, 11, 44);
		$dorm[20] = $this->building(20, 11, 44);
		$dorm[21] = $this->building(21, 11, 44);
		$total    = array();
		for ($i = 1; $i <= 21; $i++) {
			$total = array_merge($total, $dorm[$i]);
		}
		// var_dump($total);
		$rand = $this->rand($total, $amount);
		foreach ($rand as $key => $value) {
			echo '<pre>';
			echo $key + 1 . ':' . $value . "\n";
			echo '</pre>';
		}
	}

	/**
	 * 生成一层所有宿舍
	 */
	public function dorm($louhao = 0, $cengshu = 0, $total = 0) {
		if ($louhao < 10) {
			$louhao = '0' . $louhao;
		}
		for ($i = 1; $i <= $total; $i++) {
			if ($i < 10) {
				$num = '0' . $i;
			} else {
				$num = $i;
			}
			$dorm[$i] = $louhao . $cengshu . $num;
		}
		return $dorm;
	}
	/**
	 * 生成一栋楼的所有宿舍
	 */
	public function building($louhao = 0, $cengshu = 0, $pernum = 0) {
		for ($i = 1; $i <= $cengshu; $i++) {
			if ($i < 10) {
				$num = '0' . $i;
			} else {
				$num = $cengshu;
			}
			$dorm[$i] = $this->dorm($louhao, $num, $pernum);
		}
		$data = array();
		foreach ($dorm as $key => $value) {
			foreach ($value as $k => $v) {
				array_push($data, $v);
			}
		}
		return $data;
	}
	/**
	 * 随机抽取宿舍
	 *
	 * @param [type]  $data [description]
	 * @param [type]  $num  [description]
	 * @return [type]       [description]
	 */
	public function rand($data, $num) {
		$rand_keys = array_rand($data, $num);
		$k         = array();
		foreach ($rand_keys as $key => $value) {
			array_push($k, $data[$value]);
		}
		return $k;
	}
}
